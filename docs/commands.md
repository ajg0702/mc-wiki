---
title: Commands
---
These are some commands that may be useful to you on the survival servers.

Things that are in `<>` are arguments. For exmaple,
if there is the command `/home <name>`, then you would use `/home house` to teleport to the home called house.
You should **not** put the `<>` in the actual command.

| Command                                 | Description                                                                                   |
|-----------------------------------------|-----------------------------------------------------------------------------------------------|
| `/sethome <name>`                       | Set a home at the place you're standing                                                       |
| `/home <name>`                          | Teleport to a home                                                                            |
| `/delhome <name>`                       | Delete a home                                                                                 |
| `/playershop`<br/>`/pshop`              | Open the player shop.<br/>In the player shop, people can put things on sale for others to buy |
| `/pshop sell <quantity> <price>`        | Sell the item youre holding in the player shop                                                |
| `/stars`                                | Open the stars menu to buy chat stars                                                         |
| `/shop`                                 | Open the server shop. Here you can buy or sell a limited set of items to the server itself.   |
| `/spawn`                                | Teleport to the server spawn                                                                  |
| `/lobby`                                | Teleport to the lobby where you can select a different server                                 |
| `/sellgui`<br/>`/sellg`<br/>`/sg`       | Open a menu where everything you put in it will be sold in `/shop`                            |
| `/money`<br/>`/balance`<br/>`/bal`      | See how much money you have                                                                   |
| `/balance <player>`<br/>`/bal <player>` | See how much money another player has                                                         |
| `/baltop`                               |   See the players with the most money  |


If there are any more commands you think should be on this list, let me know!
