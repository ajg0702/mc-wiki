---
title: Rules
---
These are the rules of the server.

If you ever find someone is breaking the rules when I am offline, you can either dm me on [discord](https://redclock.fun/discord) (discord is preferred), or send me an email: reports@redclock.fun (if you send me an email, it'd be nice if you could send some proof)

## The rules

### Global rules
These rules apply to all servers (unless otherwise noted)

#### 1. Do not be overly annoying
   * This includes:
       1. Spamming
       2. Killing for no reason
       3. Making lots of noise around someone just to annoy them
#### 2. Do not use programs to gain an unfair advantage or cheat (hacks).
   * The clay diamond trick (and other similar tricks to know where ores are) is counted as an unfair advantage
   * Anything that automates tasks (e.g. builds/breaks for you) is not allowed.
#### 3. Do not be toxic or rude
#### 4. Use english
   * In order to make sure the chat can be moderated properly, please use english.

### Survival rules

#### 1. Do not claim grief.
   * Claim griefing can include the following:
      1. Claiming something that isn't yours
         * If you are given permission by the builder, you may claim it, however that permission may be revoked at any time, and if you refuse to remove your claim, it could lead to you being banned.
      2. If you want to remove someone's house from your claim, you must give them time to move out.
         * You must give them at least a week in order to move out and take their stuff with them.
            * This notification should be somewhere that I can check later, such as in chat, PMs in the server, /mail in the server, or signs.
         * Just untrusting them and breaking their house is punishable.
         * If the player is inactive, you should attempt to contact them in some way. If you are unable to, let aj know and he might move their house somewhere else.
      3. Breaking things inside of claims that you were trusted to
      4. Breaking things or building unpleasant things directly outside of a person's claim
#### 2. Simple autoclickers are allowed for farms only
   * Auto clickers that only click with an interval are allowed
   * No triggerbot, or aimbots are allowed, even for farms
   * Autoclickers are still not allowed for pvp.
#### 3. Do not abuse shop bugs
   * If you are able to buy something, do someting simple, then sell it for a profit, that is a shop bug.
   * If you are caught abusing this, your balance will be reset, and any suspect items will be removed.
   * If you report the bug, you will be rewarded based on how big the bug is.
#### 4. The enderdragon loot belongs to the person who spawned it
   * If an agreement is made before spawning, that agreement applies instead
     * Agreements must either be made in chat or through dms in the server, so I can verify them.
   * If multiple people placed crystals, then those people split the loot
#### 5. Do not claim around common areas
   * This includes things such as spawn, and the main end island.
   * On the main end island, any claims must be *outside* the gateway ring. The gateway portals must also be unclaimed.
      * Things around this may be removed if they are considered ugly.
   * The __only__ exception to this rule is if you get the ok from aj, which he only gives if it is for community use.
#### 6. Automatic raid farms are too OP
   * The automatic raid farm is too OP, and is not allowed on the server. 
   * Fighting raids is still fine, but the automatic raid farm is not.
### Creative rules
#### 1. Do not build anything offensive
#### 2. Nothing phallic

## Punishments
These punishments increase with each offense. Offenses can be different rules broken, or the same rule.

These punishments are not set in stone, and may vary case-to-case.

These lengths apply to both mutes and bans

1. 1 day
2. 7 day
3. 30 day
4. 90 day
5. 1 year
6. permanant
