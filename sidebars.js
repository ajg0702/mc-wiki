module.exports = {
  mc: [
    {
      type: 'category',
      label: 'Minecraft',
      items: [
        'overview',
        'rules',
        'commands',
        {
          type: 'link',
          label: 'Ender Dragons',
          href: 'https://docs.google.com/spreadsheets/d/1U624PSfk8KsM7j42uOZ-rIDfb9JMoik7R6brhvvEbtY/edit#gid=0'
        }
      ]
    }
  ]
};
