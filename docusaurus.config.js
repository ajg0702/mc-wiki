var path = require('path');
module.exports = {
  title: "Red Clock Minecraft Wiki",
  tagline: 'Find useful information about the Red Clock minecraft server',
  url: 'https://mc.redclock.fun',
  plugins: [path.resolve(__dirname, 'plugin-arc')],
  baseUrl: '/',
  onBrokenLinks: 'warn',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'https://redclock.fun/img/red_clock.png',
  organizationName: 'ajg0702', // Usually your GitHub org/user name.
  projectName: 'plugin-docs', // Usually your repo name.
  themeConfig: {
    sidebarCollapsible: true,
    hideableSidebar: true,
    navbar: {
      hideOnScroll: true,
      title: 'Red Clock MC',
      logo: {
        alt: 'Red Clock',
        src: 'https://redclock.fun/img/red_clock.png',
      },
      items: [
        {
          label: 'Wiki',
          position: 'left',
          to: '/overview'
        }
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Links',
          items: [
            {
              label: 'Discord',
              href: 'https://redclock.fun/discord',
            }
          ],
        },
        {
          title: 'ㅤ',
          items: [],
        },
        {
          title: 'Attribution',
          items: [
            {
              label: "Some icons from flaticon.com",
              href: "https://flaticon.com"
            }
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Aiden Geiss. All rights reserved.`,
    },
    "colorMode": {
      "defaultMode": "dark",
      "disableSwitch": false,
      "respectPrefersColorScheme": true,
      "switchConfig": {
        "darkIcon": "🌜",
        "darkIconStyle": {},
        "lightIcon": "🌞",
        "lightIconStyle": {}
      }
    },
    announcementBar: {},
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl:
            'https://gitlab.com/ajg0702/mc-wiki/-/edit/master',
          showLastUpdateAuthor: true,
          showLastUpdateTime: true,
          routeBasePath: "/",
        },
        blog: {
          showReadingTime: true,
          editUrl:
            'https://gitlab.com/ajg0702/mc-wiki/-/edit/master',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
